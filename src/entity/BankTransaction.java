package entity;

import java.time.Instant;

public class BankTransaction {

    private String transactionName;
    private double amount;
    private String senderIban;
    private String receiverIban;
    private String comment;
    private Instant date;

    public BankTransaction(String transactionName, double amount, String senderIban, String receiverIban, String comment, Instant date) {
        this.transactionName = transactionName;
        this.amount = amount;
        this.senderIban = senderIban;
        this.receiverIban = receiverIban;
        this.comment = comment;
        this.date = date;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getSenderIban() {
        return senderIban;
    }

    public void setSenderIban(String senderIban) {
        this.senderIban = senderIban;
    }

    public String getReceiverIban() {
        return receiverIban;
    }

    public void setReceiverIban(String receiverIban) {
        this.receiverIban = receiverIban;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "BankTransaction{" +
                "transactionName='" + transactionName + '\'' +
                ", amount=" + amount +
                ", senderIban='" + senderIban + '\'' +
                ", receiverIban='" + receiverIban + '\'' +
                ", comment='" + comment + '\'' +
                ", date=" + date +
                '}';
    }
}
