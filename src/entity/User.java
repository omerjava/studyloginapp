package entity;

public class User {

    private String name;
    private String Iban;
    private int pincode;
    private String accountType;
    private double balance;

    public User(String name, String iban, int pincode, String accountType, int balance) {
        this.name = name;
        Iban = iban;
        this.pincode = pincode;
        this.accountType = accountType;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIban() {
        return Iban;
    }

    public void setIban(String iban) {
        Iban = iban;
    }

    public int getPincode() {
        return pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", Iban='" + Iban + '\'' +
                ", pincode=" + pincode +
                ", accountType='" + accountType + '\'' +
                ", balance=" + balance +
                '}';
    }
}
