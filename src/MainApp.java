import entity.User;

import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {

        User user1 = new User("John doe", "BE12125478962584", 5478, "Individual", 5000);
        User user2 = new User("Jack doe", "BE12125478962580", 5778, "Savings", 7000);
        User user3 = new User("Kate doe", "BE12125478962587", 5278, "Corporate", 508800);
        User user4 = new User("Hans doe", "BE12125478962586", 9478, "Commercial", 50080);
        User user5 = new User("Ahmet doe", "BE12125478962589", 5479, "Individual", 500);

        User[] users = new User[5];

        users[0] = user1;
        users[1] = user2;
        users[2] = user3;
        users[3] = user4;
        users[4] = user5;


        System.out.println("Welcome to Bank");

        String userIban = "";
        boolean isIbanNotCorrect = true;
        while(isIbanNotCorrect) {
            System.out.println("Enter your Iban no for login");

            Scanner scannerIban = new Scanner(System.in);
            userIban = scannerIban.nextLine();

            for (User bankUser : users) {
                if (userIban.equalsIgnoreCase(bankUser.getIban())) {
                    isIbanNotCorrect=false;
                }
            }
        }




        boolean isPincodeNotCorrect = true;
        int i = 0;
        while(isPincodeNotCorrect && i<3) {
            System.out.println("Enter your pincode for login (you can try max 3 times)");
            Scanner scannerPincode = new Scanner(System.in);
            int userPincode = scannerPincode.nextInt();

            for (User bankUser : users) {


                if (userIban.equalsIgnoreCase(bankUser.getIban())
                ) {
                    if (userPincode == bankUser.getPincode()) {
                        System.out.println("Your iban and pincode is correct you logged in!");
                        isPincodeNotCorrect=false;
                    }
                    else {
                        i++;
                    }
                }

            }


            if(i==3){
                System.out.println("You tried 3 times wrong pincode, your account is blocked");
                break;
            }
        }




    }
}
